from math import floor
from typing import Dict, List, overload

from otadini.data.source import DataSource
from otadini.data.universe import Universe
from otadini.broker.interface import Broker
from otadini.portfolio.interface import Portfolio
from otadini.event.observer import EventObserver
from otadini.core.types import EventType, Event, OrderCreatedEvent, TargetWeightEvent, OrderWithType, order_builder, Quote
from otadini.event.emit import EventEmitter


class ExecutionContext(EventObserver):
    def __build_orders(self, target_weights: Dict[str, float]) -> List[OrderWithType]:
        if not self.portfolio:
            return []

        curr_alloc: Dict[str, int] = self.portfolio.get_allocations()
        total_value: float = self.portfolio.get_total_value()
        orders: List[OrderWithType] = []
        for symbol in target_weights:
            target_val: float = target_weights[symbol] * total_value
            curr_val: float = self.portfolio.get_symbol_value(symbol)
            diff_val: float = target_val - curr_val
            quote: Quote = self.ds.get_latest_quote(symbol)
            target_shares: int = 0
            if diff_val > 0:
                target_shares = floor(diff_val / quote.ask)
            elif diff_val < 0:
                target_shares = floor(diff_val / quote.bid)
            if target_shares != 0:
                orders.append(order_builder(symbol, target_shares))
        return orders

    @overload
    def update(self, event: TargetWeightEvent) -> None: ...

    @overload
    def update(self, event: Event) -> None: ...

    def update(self, event) -> None:
        if event.event_type == EventType.TARGET_WEIGHT_EVENT:
            orders = self.__build_orders(event.target_weights)
            ev = OrderCreatedEvent(orders)
            self.ee.emit(ev)
        return

    def __init__(
        self,
        ds: DataSource,
        universe: Universe,
        brkr: Broker,
        port: Portfolio,
        ee: EventEmitter,
    ):
        self.ee: EventEmitter = ee
        self.ds: DataSource = ds
        self.universe: Universe = universe
        self.brkr: Broker = brkr
        self.portfolio: Portfolio = port

        self.ee.subscribe(self)
        self.ee.subscribe(self.brkr)
        self.ee.subscribe(self.portfolio)
        return
