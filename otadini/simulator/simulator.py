from datetime import datetime

from otadini.portfolio.interface import Portfolio
from otadini.data.source import DataSourceIterable


class SimulatorContext:
    def __init__(
        self,
        start_dt: datetime,
        end_dt: datetime,
        portfolio: Portfolio,
        ds: DataSourceIterable,
    ):
        self.start_dt: datetime = start_dt
        self.end_dt: datetime = end_dt
        self.portfolio: Portfolio = portfolio
        self.ds: DataSourceIterable = ds
        return


class Simulator:
    def run(self) -> None:
        while self.ctxt.ds.has_next():
            self.ctxt.ds.next()
        return

    def __init__(self, ctxt: SimulatorContext):
        self.ctxt: SimulatorContext = ctxt
        return
