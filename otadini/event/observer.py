from abc import ABCMeta, abstractmethod

from otadini.core.types import Event


class EventObserver(metaclass=ABCMeta):
    @abstractmethod
    def update(self, event: Event) -> None:
        pass
