from otadini.event.observer import EventObserver
from otadini.core.types import Event


class EventEmitter:
    def emit(self, event: Event):
        for o in self.observers:
            o.update(event)
        return

    def subscribe(self, observer: EventObserver):
        self.observers.append(observer)
        return

    def __init__(self):
        self.observers = []
        return
