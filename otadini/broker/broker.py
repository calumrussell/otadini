from abc import ABCMeta, abstractmethod
from typing import Optional, overload

from otadini.data.source import DataSource
from otadini.broker.interface import Broker
from otadini.event.emit import EventEmitter
from otadini.core.types import Event, EventType, OrderCreatedEvent, TradeSuccessEvent, OrderWithType, OrderType, Quote


class SimulatedBroker(Broker):

    @overload
    def update(self, event: OrderCreatedEvent) -> None: ...

    @overload
    def update(self, event: Event) -> None: ...

    def update(self, event) -> None:
        if event.event_type == EventType.ORDER_CREATED_EVENT:
            for order in event.orders:
                self.execute_order(order)
        return

    def get_latest_quote(self, symbol: str) -> Quote:
        return self.ds.get_latest_quote(symbol)

    def execute_order(self, order: OrderWithType) -> None:
        price: float = 0.0

        if order.order_type == OrderType.MARKET_BUY:
            price = self.get_latest_quote(order.symbol).ask
            trade = TradeSuccessEvent(order, price)
            self.ee.emit(trade)
        elif order.order_type == OrderType.MARKET_SELL:
            price = self.get_latest_quote(order.symbol).bid
            trade = TradeSuccessEvent(order, price)
            self.ee.emit(trade)
        return

    def __init__(self, ds: DataSource, ee: EventEmitter):
        super().__init__()
        self.ds: DataSource = ds
        self.ee: EventEmitter = ee
        return
