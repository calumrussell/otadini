from abc import ABCMeta, abstractmethod
from typing import Optional

from otadini.core.types import OrderWithType, Quote
from otadini.event.observer import EventObserver


class Broker(EventObserver, metaclass=ABCMeta):
    @abstractmethod
    def execute_order(self, order: OrderWithType) -> None:
        pass

    @abstractmethod
    def get_latest_quote(self, symbol: str) -> Quote:
        pass
