cimport cython
from cpython.datetime cimport datetime


cpdef enum EventType:
    TRADE_CREATED_EVENT,
    SIM_PRICE_UPDATE_EVENT,
    TARGET_WEIGHT_EVENT,
    ORDER_CREATED_EVENT,
    TRADE_SUCCESS_EVENT,


cdef class Event:
    cdef EventType _event_type

    @property
    def event_type(self) -> EventType:
        return self._event_type

    def __init__(self, event_type: EventType):
        self._event_type = event_type
        return

cdef class SimPriceUpdateEvent(Event):
    cdef datetime _date
    cdef dict _prices

    @property
    def date(self) -> datetime:
        return self._date

    @property
    def prices(self) -> dict:
        return self._prices

    def __init__(self, datetime date, dict prices):
        super().__init__(EventType.SIM_PRICE_UPDATE_EVENT)
        self._date = date
        self._prices = prices
        return

cdef class TargetWeightEvent(Event):
    cdef dict _target_weights

    @property
    def target_weights(self) -> dict:
        return self._target_weights

    def __init__(self, dict target_weights):
        super().__init__(EventType.TARGET_WEIGHT_EVENT)
        self._target_weights = target_weights
        return

cdef class OrderCreatedEvent(Event):
    cdef list _orders

    @property
    def orders(self) -> list:
        return self._orders

    def __init__(self, list orders):
        super().__init__(EventType.ORDER_CREATED_EVENT)
        self._orders = orders
        return

cdef class TradeSuccessEvent(Event):
    cdef OrderWithType _order
    cdef float _price

    @property
    def order(self) -> OrderWithType:
        return self._order

    @property
    def price(self) -> float:
        return self._price

    def __init__(self, OrderWithType order, float price):
        ##order is OrderWithType
        super().__init__(EventType.TRADE_SUCCESS_EVENT)
        self._order = order
        self._price = price
        return

cdef class Order:
    cdef str _symbol
    cdef int _shares

    @property
    def symbol(self) -> str:
        return self._symbol

    @property
    def shares(self) -> int:
        return self._shares

    def __init__(self, str symbol, int shares):
        self._symbol = symbol
        self._shares = shares
        return


cpdef enum OrderType:
    MARKET_SELL,
    MARKET_BUY


cdef class OrderWithType(Order):
    cdef OrderType _order_type

    @property
    def order_type(self) -> OrderType:
        return self._order_type

    def __init__(self, str symbol, int shares, OrderType order_type):
        super().__init__(symbol, shares)
        self._order_type = order_type
        return


cdef class SellOrder(OrderWithType):

    def __init__(self, str symbol, int shares):
        super().__init__(symbol, shares, OrderType.MARKET_SELL)
        return


cdef class BuyOrder(OrderWithType):

    def __init__(self, str symbol, int shares):
        super().__init__(symbol, shares, OrderType.MARKET_BUY)
        return

cpdef OrderWithType order_builder(str symbol, int shares_req):
    if shares_req > 0:
        return BuyOrder(symbol, shares_req)
    elif shares_req < 0:
        return SellOrder(symbol, shares_req)
    else:
        raise ValueError("Should not pass no trade to OrderFactory")

cdef class Quote:
    cdef float _bid
    cdef float _ask
    cdef datetime _dt

    @property
    def bid(self) -> float:
        return self._bid

    @property
    def ask(self) -> float:
        return self._ask

    @property
    def dt(self) -> datetime:
        return self._dt

    def __init__(self, float bid, float ask, datetime dt):
        self._bid = bid
        self._ask = ask
        self._dt = dt
        return

cdef class StockQuote(Quote):
    cdef str _symbol 

    @property
    def symbol(self) -> str:
        return self._symbol

    def __init__(self, float bid, float ask, datetime dt, str symbol):
        super().__init__(bid, ask, dt)
        self._symbol = symbol
        return
