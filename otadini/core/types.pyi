from enum import Enum
from typing import List, Dict
from datetime import datetime

class EventType(Enum):
    TRADE_CREATED_EVENT = 0
    SIM_PRICE_UPDATE_EVENT = 1
    TARGET_WEIGHT_EVENT = 2
    ORDER_CREATED_EVENT = 3
    TRADE_SUCCESS_EVENT = 4

class Event:

    @property
    def event_type(self) -> EventType: ...

    def __init__(self, event_type: EventType) -> None: ...

class SimPriceUpdateEvent(Event):

    @property
    def date(self) -> datetime: ...

    @property
    def prices(self) -> Dict[str, float]: ...

    def __init__(self, date: datetime, prices: Dict[str, float]) -> None: ...

class TargetWeightEvent(Event):

    @property
    def target_weights(self) -> Dict[str, float]: ...
    
    def __init__(self, target_weights: Dict[str, float]) -> None: ...

class OrderCreatedEvent(Event):

    @property
    def order(self) -> List[OrderWithType]: ...

    def __init__(self, orders: List[OrderWithType]) -> None: ...

class TradeSuccessEvent(Event):

    @property
    def order(self) -> OrderWithType: ...

    @property
    def price(self) -> float: ...

    def __init__(self, order: OrderWithType, price: float) -> None: ...

class Order:
    @property
    def symbol(self) -> str: ...

    @property
    def shares(self) -> int: ...

    def __init__(self, symbol: str, shares: int) -> None: ...


class OrderType(Enum):
    MARKET_SELL=0
    MARKET_BUY=1


class OrderWithType(Order):
    @property
    def order_type(self) -> OrderType: ...

    def __init__(self, symbol: str, shares: int, order_type: OrderType) -> None: ...

class SellOrder(OrderWithType):

    def __init__(self, symbol: str, shares: int) -> None: ...

class BuyOrder(OrderWithType):

    def __init__(self, symbol: str, shares: int) -> None: ...

def order_builder(symbol: str, shares_req: int) -> OrderWithType: ...

class Quote:

    @property
    def bid(self) -> float: ...

    @property
    def ask(self) -> float: ...

    @property
    def dt(self) -> datetime: ...

    def __init__(self, bid: float, ask: float, dt: datetime) -> None: ...

class StockQuote(Quote):

    @property
    def symbol(self) -> str: ...

    def __init__(self, bid: float, ask: float, dt: datetime, symbol: str) -> None: ...

