from abc import ABCMeta, abstractmethod
from typing import Dict


class BaseTradingSystem(metaclass=ABCMeta):
    @abstractmethod
    def calculate_weights(self) -> Dict[str, float]:
        pass
