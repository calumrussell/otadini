from typing import Dict, List, overload
from abc import ABCMeta, abstractmethod
from datetime import datetime

from otadini.trading.base import BaseTradingSystem
from otadini.event.observer import EventObserver
from otadini.core.types import EventType, Event, TargetWeightEvent, SimPriceUpdateEvent
from otadini.event.emit import EventEmitter


class TradingSystem(BaseTradingSystem, EventObserver):
    """Contains the logic for calculating what orders
    we need to generate given the state of the market,
    and the current state of our portfolio.

    We should be able to have multiple TradingSystem running
    simultaneously but only one Portfolio that is shared
    across all.

    Any data dependencies should be hardcoded into the system.
    The only dependency within the trading application should
    be to a portfolio which can execute changes in target
    weights.
    """

    @overload
    def update(self, event: SimPriceUpdateEvent) -> None: ...

    @overload
    def update(self, event: Event) -> None: ...

    def update(self, event) -> None:
        if event.event_type == EventType.SIM_PRICE_UPDATE_EVENT:
            target_weights = self.calculate_weights()
            ev: TargetWeightEvent = TargetWeightEvent(target_weights)
            self.ee.emit(ev)
        return

    def __init__(self, ee: EventEmitter):
        self.ee = ee
        self.ee.subscribe(self)
        return
