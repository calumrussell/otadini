from abc import ABCMeta, abstractmethod
from typing import Dict

from otadini.event.observer import EventObserver


class Portfolio(EventObserver, metaclass=ABCMeta):
    @abstractmethod
    def get_allocations(self) -> Dict[str, int]:
        pass

    @abstractmethod
    def get_total_value(self) -> float:
        pass

    @abstractmethod
    def get_symbol_value(self, symbol: str) -> float:
        pass
