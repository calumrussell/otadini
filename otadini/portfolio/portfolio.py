from typing import List, Dict, Callable, overload
from collections import OrderedDict
from math import floor

from otadini.data.fetcher import MarketPriceFetcher
from otadini.data.universe import Universe
from otadini.core.types import EventType, Event, TradeSuccessEvent, Order, Quote
from otadini.event.observer import EventObserver


class PortfolioState:
    def __init__(
        self, allocations: Dict[str, int], cash: float, price_api: MarketPriceFetcher
    ) -> None:
        self.price_api = price_api
        self.cash = cash
        self.symbols: List[str] = list(allocations.keys())
        self.prices: List[Quote] = []
        self.__load_prices()
        self.shares: List[int] = list(allocations.values())
        self.values: List[float] = [i * j.ask for i, j in zip(self.shares, self.prices)]
        self.total_value: float = sum(self.values) + self.cash
        self.weights: List[float] = [i / self.total_value for i in self.values]
        return

    def __load_prices(self) -> None:
        get_quote: Callable[[str], Quote] = lambda x: self.price_api.get_latest_quote(x)
        for s in self.symbols:
            self.prices.append(get_quote(s))
        return

    def get_position(self, symbol) -> int:
        return self.symbols.index(symbol)

    def get_value(self, symbol) -> float:
        pos = self.get_position(symbol)
        return self.values[pos]

    def get_price(self, symbol) -> Quote:
        pos = self.get_position(symbol)
        return self.prices[pos]

    def get_shares(self, symbol) -> int:
        pos = self.get_position(symbol)
        return self.shares[pos]

    def get_allocations(self) -> Dict[str, int]:
        return {i: j for i, j in zip(self.symbols, self.shares)}


class SimPortfolio(EventObserver):
    def __init__(self, universe: Universe, price_api: MarketPriceFetcher):
        self.universe = universe
        self.price_api = price_api
        self.initial_cash: float = 1e6

        allocations: Dict[str, int] = self.__calculate_initial_allocation()
        self.state: PortfolioState = PortfolioState(
            allocations, self.initial_cash, self.price_api
        )
        return

    def get_allocations(self) -> Dict[str, int]:
        return self.state.get_allocations()

    def get_total_value(self) -> float:
        return self.state.total_value

    def get_symbol_value(self, symbol: str) -> float:
        return self.state.get_value(symbol)

    @overload
    def update(self, event: TradeSuccessEvent) -> None: ...

    @overload
    def update(self, event: Event) -> None: ...

    def update(self, event) -> None:
        if event.event_type == EventType.TRADE_SUCCESS_EVENT:
            order: Order = event.order
            price: float = event.price

            trade_value: float = order.shares * price
            new_cash: float = self.state.cash - trade_value

            curr_alloc: int = self.state.get_shares(order.symbol)
            new_alloc: int = curr_alloc + order.shares

            alloc_copy: Dict[str, int] = self.state.get_allocations()
            alloc_copy[order.symbol] = new_alloc
            self.state = PortfolioState(alloc_copy, new_cash, self.price_api)
        return

    def __calculate_initial_allocation(self) -> Dict[str, int]:
        assets: List[str] = self.universe.assets
        allocations: Dict[str, int] = OrderedDict()
        for symbol, alloc in zip(assets, [0] * len(assets)):
            allocations[symbol] = alloc
        return allocations
