from typing import Dict, List, overload
from abc import ABCMeta, abstractmethod

from itertools import groupby
from functools import reduce
from datetime import datetime
from collections import OrderedDict
import csv

from otadini.event.observer import EventObserver
from otadini.core.types import EventType, Event, SimPriceUpdateEvent, Quote
from otadini.event.emit import EventEmitter


class DataSource(EventObserver, metaclass=ABCMeta):

    @overload
    def update(self, event: SimPriceUpdateEvent) -> None: ...

    @overload
    def update(self, event: Event) -> None: ...

    def update(self, event) -> None:
        if event.event_type == EventType.SIM_PRICE_UPDATE_EVENT:
            pass
        return

    @abstractmethod
    def get_latest_quote(self, symbol: str) -> Quote:
        pass

    def __init__(self, ee: EventEmitter):
        self.ee = ee
        self.ee.subscribe(self)
        return


class DataSourceIterable(metaclass=ABCMeta):
    @abstractmethod
    def next(self) -> None:
        pass

    @abstractmethod
    def has_next(self) -> bool:
        pass
