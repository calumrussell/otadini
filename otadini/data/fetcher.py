from abc import ABCMeta, abstractmethod

from otadini.core.types import Quote


class MarketPriceFetcher(metaclass=ABCMeta):
    @abstractmethod
    def get_latest_quote(self, symbol: str) -> Quote:
        pass
