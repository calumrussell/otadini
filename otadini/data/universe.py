from abc import ABCMeta, abstractmethod
from typing import List


class Universe(metaclass=ABCMeta):
    @property
    @abstractmethod
    def assets(self) -> List[str]:
        pass

    def __init__(self, assets: List[str]):
        self._assets: List[str] = assets
        return


class StaticUniverse(Universe):
    @property
    def assets(self) -> List[str]:
        return self._assets

    def __init__(self, assets: List[str]):
        super().__init__(assets)
        return
