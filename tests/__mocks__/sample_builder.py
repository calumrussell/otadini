import csv
import numpy as np
import math

res = []

tickers = ["ABC", "BCD", "CDE", "DEF", "EFG", "FGH", "GHI", "HIJ", "IJK", "JKL"]
for t in tickers:
    price = np.random.uniform(1, 100, 1)[0]
    vol = np.random.uniform(0.1, 0.3, 1)[0]
    log_price = math.log(price)
    prices = np.random.normal(log_price, vol, 10000)
    bid = []
    ask = []
    for i, p in enumerate(prices):
        if i % 2 == 0:
            bid.append(math.exp(p))
        else:
            ask.append(math.exp(p))

    start_date = 100
    dates = range(start_date, start_date + len(prices))
    for b, a, d in zip(bid, ask, dates):
        bid_quote = {"symbol": t, "quote_type": "bid", "price": round(b, 2), "date": d}
        ask_quote = {"symbol": t, "quote_type": "ask", "price": round(a, 2), "date": d}
        res.append(bid_quote)
        res.append(ask_quote)

with open("longer_sample.csv", "w") as f:
    w = csv.writer(f)
    w.writerow(res[0].keys())
    for r in res:
        w.writerow(r.values())
