"""

  * How do we handle various types of allocation models? How do we handle 
  a model with constant allocations? How do we do this, and handle dynamic 
  allocations?

  * Moved the structure to pub/sub. The main publisher is the DataSource object
  which passes all price updates to the subscribers. The TradingSystem
  subscribes to DataSource and emits Orders.

  * The dependencies of the TradingSystem are unclear because we need to know
  the current state of the Portfolio before we can create Order. We also have
  an overlapping concern which is the RiskModel, this is subtly different
  from the TradingSystem in that it may need to operate outside of a pure
  alpha-generating trade.

  * Portfolio dispatches orders to the Broker. Only responsibility of the
  TradingSystem is to take prices and current portfolio state as an input
  and generate trades (ignoring issues of slippage/execution quality for now).

"""

import unittest
import csv
from datetime import datetime
from typing import Dict

from otadini.data.source import DataSource, DataSourceIterable
from otadini.portfolio.portfolio import SimPortfolio
from otadini.trading.trading import TradingSystem
from otadini.simulator.simulator import SimulatorContext, Simulator
from otadini.execution.execution import ExecutionContext
from otadini.core.types import Order, SimPriceUpdateEvent, StockQuote
from otadini.broker.broker import SimulatedBroker
from otadini.data.universe import StaticUniverse
from otadini.event.emit import EventEmitter


class CSVFlatDataSource(DataSource, DataSourceIterable):
    def get_latest_quote(self, symbol: str) -> StockQuote:
        date = list(self.data.keys())[self.pos]
        return self.data[date][symbol]

    def __build_raw(self):
        self.raw_data = []
        with open(self.file_name, "r") as f:
            contents = csv.DictReader(f)
            for row in contents:
                self.raw_data.append(row)
        return

    def has_next(self) -> bool:
        return self.pos < len(list(self.data.keys()))

    def next(self):
        date = list(self.data.keys())[self.pos]
        curr = self.data[date]
        self.ee.emit(SimPriceUpdateEvent(date,curr))
        self.pos += 1
        return

    def __format_to_data(self):
        from itertools import groupby
        from collections import OrderedDict

        self.data: OrderedDict[datetime, List[StockQuote]] = {}
        grouped = groupby(self.raw_data, lambda x: x["symbol"] + "-" + x["date"])
        for key, group in grouped:
            bid: float = 0.0
            ask: float = 0.0
            symbol: str = ""
            date: datetime = datetime.fromtimestamp(-1)
            for price in group:
                if price["quote_type"] == "ask":
                    ask = float(price["price"])
                elif price["quote_type"] == "bid":
                    bid = float(price["price"])
                else:
                    raise ValueError("Unkown quote_type")
                date: datetime = datetime.fromtimestamp(int(price["date"]))
                symbol: str = str(price["symbol"])
            quote = StockQuote(bid=bid, ask=ask, dt=date, symbol=symbol)

            if date not in self.data:
                self.data[date] = {}
            self.data[date][symbol] = quote
        return

    def __init__(self, file_name: str, ev: EventEmitter):
        super().__init__(ev)
        self.pos = 0
        self.file_name = file_name
        self.__build_raw()
        self.__format_to_data()
        return


class FixedWeightTradingSystem(TradingSystem):
    def calculate_weights(self):
        return self.weights

    def __init__(self, weights: Dict[str, float], ev: EventEmitter):
        super().__init__(ev)
        self.weights = weights
        return


class TestMain(unittest.TestCase):
    def test_main(self):
        ev = EventEmitter()
        ds = CSVFlatDataSource("./tests/__mocks__/test.csv", ev)
        fws = FixedWeightTradingSystem({"ABC": 0.5, "BCD": 0.5}, ev)
        universe = StaticUniverse(["ABC", "BCD"])
        brkr = SimulatedBroker(ds, ev)

        portfolio = SimPortfolio(universe, ds)
        exec_ctxt = ExecutionContext(ds, universe, brkr, portfolio, ev)

        sim_ctxt = SimulatorContext(
            start_dt=datetime.fromtimestamp(100),
            end_dt=datetime.fromtimestamp(102),
            portfolio=portfolio,
            ds=ds,
        )
        sim = Simulator(sim_ctxt)
        sim.run()
        return
